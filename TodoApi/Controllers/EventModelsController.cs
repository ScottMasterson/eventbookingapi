﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.DataModels;

namespace TodoApi.Controllers
{
    [Route("api/Events")]
    [ApiController]
    public class EventModelsController : ControllerBase
    {
        private readonly EventContext _context;
        private List<Car> cars;

        private const int BASE_COST_PER_HOUR = 50;
        private const int BASE_COST_PER_PERSON = 10;
        public EventModelsController(EventContext context)
        {
            _context = context;
            cars = _context.Cars.ToList();
        }

        // GET: api/EventModels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EventModel>>> GetEvents()
        {
            return await _context.Events.ToListAsync();
        }

        // GET: api/EventModels
        [HttpGet("EventDetails/{id}")]
        public async Task<ActionResult<IEnumerable<EventCarList>>> EventDetails(long id)
        {
            var eventCars = await _context.EventCarLists.ToListAsync();

            eventCars = eventCars.Where(x => x.EventID == id).ToList(); ;

            return eventCars;
        }


        // GET: api/EventModels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EventModel>> GetEventModel(long id)
        {
            var eventModel = await _context.Events.FindAsync(id);

            if (eventModel == null)
            {
                return NotFound();
            }

            return eventModel;
        }

        // PUT: api/EventModels/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEventModel(long id, EventModel eventModel)
        {
            if (id != eventModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(eventModel).State = EntityState.Modified;
            RemoveEventCarList(_context.EventCarLists.Where(x => x.Id == id).ToList());

            List<EventCarList> eclList = BuildTrainList(eventModel);
            eventModel.Cost = CalculateCost(eventModel, eclList.Count);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EventModels
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<EventModel>> PostEventModel(EventModel eventModel)
        {
            DateTime dt = eventModel.EventDate;
          
            if (_context.Events.Any(x => x.EventDate == dt)) {
                eventModel.Messages = "The train has already been booked for that day";
                return CreatedAtAction("GetEventModel", new { id = 0}, eventModel);
            }
            //save  and get primary key
            _context.Events.Add(eventModel);
            await _context.SaveChangesAsync();

            //build train list and calculate cost
            List<EventCarList> eclList = BuildTrainList(eventModel);
            eventModel.Cost = CalculateCost(eventModel,eclList.Count);
            _context.EventCarLists.AddRange(eclList);

            await _context.SaveChangesAsync();
            //await _context.Entry(eventModel).Context.SaveChangesAsync();

            return CreatedAtAction("GetEventModel", new { id = eventModel.Id, cost = eventModel.Cost }, eventModel);
        }

        private float CalculateCost(EventModel eventModel, int numTraines) {
            float multiplier = numTraines * 0.25f;
            return ((BASE_COST_PER_HOUR * eventModel.Length) + (BASE_COST_PER_PERSON * eventModel.NumOfGuests)) * multiplier;
        }
        private List<EventCarList> BuildTrainList(EventModel eventModel) {

           
            List<EventCarList> eclList = new List<EventCarList>();
            EventCarList ecl = new EventCarList();
            long eventID = eventModel.Id;

            //Calculate number of sitting cars 
            Car car = cars.Where(x => x.Name.ToLower() == "sitting").FirstOrDefault();
            ecl.CarID = car.Id;
            ecl.Quantity = (int)Math.Ceiling((double)eventModel.NumOfGuests / car.MaxCapacity);
            ecl.EventID = eventModel.Id;
            eclList.Add(ecl);

            //Calculate number of dining cars needed
            if (eventModel.UsingDining)
            {
                ecl = new EventCarList();
                car =  cars.Where(x => x.Name.ToLower() == "dining").FirstOrDefault();
                ecl.CarID = car.Id;
                ecl.Quantity = (int)Math.Ceiling((double)eventModel.NumOfGuests / car.MaxCapacity);
                ecl.EventID = eventID;
                eclList.Add(ecl);
            }

            //only 1 caboose / train
            if (eventModel.UsingCaboose)
            {
                ecl = new EventCarList();
                ecl.CarID = cars.Where(x => x.Name.ToLower() == "caboose").FirstOrDefault().Id;
                ecl.EventID = eventID;
                eclList.Add(ecl);
            }

            //only 1 parlour car / train
            if (eventModel.UsingParlour)
            {
                ecl = new EventCarList();
                ecl.CarID = cars.Where(x => x.Name.ToLower() == "parlour").FirstOrDefault().Id;
                ecl.EventID = eventID;
                eclList.Add(ecl);
            }

            return eclList;
        }

        // DELETE: api/EventModels/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<EventModel>> DeleteEventModel(long id)
        {
            var eventModel = await _context.Events.FindAsync(id);
            if (eventModel == null)
            {
                return NotFound();
            }

            RemoveEventCarList( _context.EventCarLists.Where(x => x.Id == id).ToList());
            _context.Events.Remove(eventModel);
            await _context.SaveChangesAsync();

            return eventModel;
        }

        private bool EventModelExists(long id)
        {
            return _context.Events.Any(e => e.Id == id);
        }

        private void RemoveEventCarList(List<EventCarList> eventCarList)
        {
            foreach (EventCarList ecl in eventCarList)
            {
                _context.EventCarLists.Remove(ecl);
            }
        }
    }
}
