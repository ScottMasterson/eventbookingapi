﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.DataModels
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            //Seed car table with available vehicles
            using (var context = new EventContext(
            serviceProvider.GetRequiredService<DbContextOptions<EventContext>>()))
            {
                if (context.Cars.Any()) {
                    return;
                }
                context.Cars.AddRange(
                    new Car { 
                        Id = 1,
                        Name = "Sitting",
                        Description = "A great place to sit",
                        MaxCapacity = 40
                    },
                    new Car
                    {
                        Id = 2,
                        Name = "Dining",
                        Description = "Mmmmm delicious food",
                        MaxCapacity = 20
                    },
                    new Car
                    {
                        Id = 3,
                        Name = "Caboose",
                        Description = "For the childish side",
                        MaxCapacity = 25
                    },
                    new Car
                    {
                        Id = 4,
                        Name = "Parlour",
                        Description = "Games, Drinks and More",
                        MaxCapacity = 100
                    }
                    );
                context.SaveChanges();
            }
        }
    }
}
