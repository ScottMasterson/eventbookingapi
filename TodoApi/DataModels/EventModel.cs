﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.DataModels
{
    public class EventModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public int NumOfGuests { get; set; }
        public DateTime EventDate { get; set; }
        public string Notes{ get; set; }
        public bool UsingDining { get; set; } = false;
        public bool UsingParlour { get; set; } = false;
        public bool UsingCaboose { get; set; } = false;
        public float Cost { get; set; }
        public string Messages { get; set; } = "";
    }
}
