﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.DataModels
{
    public class EventCarList
    {
        public long Id { get; set; }
        public long  EventID { get; set; }
        public long CarID { get; set; }
        public int Quantity { get; set; } = 1;
    }
}
