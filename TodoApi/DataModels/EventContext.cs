﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TodoApi.DataModels
{
    public class EventContext : DbContext
    {
        public EventContext(DbContextOptions<EventContext> options)
          : base(options)
        {
        }

        public DbSet<EventModel> Events { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<EventCarList> EventCarLists { get; set; }
    }
}
