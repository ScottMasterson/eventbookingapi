﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.DataModels
{
    public class Car
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description{ get; set; }
        public int MaxCapacity { get; set; }
    }
}
